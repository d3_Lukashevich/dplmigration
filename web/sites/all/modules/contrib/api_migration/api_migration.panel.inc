<?php

function _api_migration_panels_list($fn)
{

    $handlers = _api_migration_select_list('page_manager_handlers', 'name');

    foreach ($handlers as $handler) {
        $panels = _api_migration_panel_get_panels_by_handler($handler);
        $pages = _api_migration_panel_get_pages_by_handler($handler);
        $display = _api_migration_panel_get_display_by_handler($handler);
        $handler->panels_pane = $panels;
        $handler->page_manager_pages = $pages;
        $handler->display = $display;
    }

    $result = array(
//        'panel' => _api_migration_panels_add_uuid(
//            _api_migration_select_list('panels_pane', 'pid')
//        ),
        'page_manager_hundlers' => $handlers,
        'page_manager_weights' => _api_migration_select_list('page_manager_weights', 'name'),
    );

    return $result;
}

/**
 * @param $table_name
 * @param $key
 * @param array $tags
 * @param array $conditions
 * @return mixed
 */
function _api_migration_select_list($table_name, $key, $tags = array(), $conditions = array())
{
    $query = db_select($table_name, 't');
    $query
        ->fields('t')
        ->addTag('translatable');

    foreach ($conditions as $condition) {
        if (!empty($condition)) {
            $query->condition($condition['field'], $condition['value'], (isset($condition['action'])) ? $condition['action'] : '=');
        }
    }

    foreach ($tags as $tag) {
        $query->addTag($tag);
    }

    return $query->execute()->fetchAllAssoc($key);
}

/**
 * @param stdClass[] $panels
 * @return stdClass[]
 */
function _api_migration_panels_add_uuid($panels)
{
    foreach ($panels as $panel) {
        if ($panel->type === 'node') {
            $configuration = unserialize($panel->configuration);
            $nid = $configuration['nid'];
            $node = node_load($nid);

            $configuration['uuid'] = $node->uuid;
            $panel->configuration = serialize($configuration);
        }
    }

    return $panels;
}

function _api_migration_panel_get_panels_by_handler($handler)
{
    $conf = $handler->conf;
    $conf = unserialize($conf);

    $panels = _api_migration_panels_add_uuid(
        _api_migration_select_list('panels_pane', 'pid', array(), array(
            array('field' => 'did', 'value' => (int) $conf['did'])
        ))
    );

    return $panels;
}

function _api_migration_panel_get_pages_by_handler($handler)
{
    $subtask = $handler->subtask;

    $pages = _api_migration_select_list('page_manager_pages', 'pid', array(), array(
       array('field' => 'name', 'value' => $subtask)
    ));

    return $pages;
}

/**
 * @param array $source
 * @return mixed
 */
function _api_migration_get_panels($source)
{
    $service_url = $source['url'] . '/' . $source['endpoint'] . '/' . 'panel/index.json';

    $curl_response = _api_migration_get_content_with_curl($service_url, $source);

    return drupal_json_decode($curl_response);
}

/**
 * @param array $import_panel
 * @param array $site_panels
 * @return array
 */
function _api_migration_compare_two_panels($import_panel, $site_panels)
{
    $compare_data = array();
    $compare_data['status'] = 'new';
    $compare_data['fields'] = array();

    if (!empty($site_panels) && isset($site_panels[$import_panel['name']])) {
        $site_panel = $site_panels[$import_panel['name']];
        $import_panel = _api_migration_prepare_panel_for_compare($import_panel);
        $site_panel = _api_migration_prepare_panel_for_compare($site_panel);
        $compare_data['status'] = 'not modified';

        foreach ( $site_panel as $key => $item) {
            if ($import_panel[$key] !== $site_panel[$key]) {
                $compare_data['status'] = 'modified';
                $compare_data['fields'][$key] = 'field-changed';
            }
        }
    }

    return $compare_data;
}

/**
 * @param array $panel
 * @return mixed
 */
function _api_migration_prepare_panel_for_compare($panel)
{
    foreach ($panel['display'] as $key => $item) {
        unset($item['did']);
        $panel['display'] = $item;
    }

    unset($panel['did']);
    $conf = unserialize($panel['conf']);
    unset($conf['did']);
    $panel['conf'] = serialize($conf);

    foreach ($panel['panels_pane'] as $key => $panel_pane) {
        unset($panel_pane['pid'], $panel_pane['did'], $panel_pane['uuid']);
        $configuration = unserialize($panel_pane['configuration']);
        unset($configuration['nid']);
        $panel_pane['configuration'] = serialize($configuration);

        $panel_data[] = $panel_pane;
    }
    $panel['panels_pane'] = $panel_data;

    return $panel;
}

/**
 * @param stdClass $handler
 * @return mixed
 */
function _api_migration_panel_get_display_by_handler($handler)
{
    $conf = unserialize($handler->conf);

    $display = _api_migration_select_list('panels_display', 'did', array(), array(
        array('field' => 'did', 'value' => $conf['did'])
    ));

    return $display;
}