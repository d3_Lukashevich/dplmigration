<?php

/**
 * Save node to database
 */
function api_migration_save_nodes()
{
    if (!isset($_POST['table'])) {
        _api_migration_info_message('Nothing to save', 'error', 'admin/config/content/api_migration');
    }

    $count = _api_migration_get_saved_nodes($_POST);
    $message = sprintf('Import successful %s inserted %s updated', $count['inserted'], $count['updated']);
    _api_migration_info_message($message, 'status', 'admin/content');
}

/**
 * @param array $inputs
 * @return array
 */
function _api_migration_get_saved_nodes($inputs = array())
{
    $selected_nodes = (isset($inputs['table'])) ? $inputs['table'] : array();

    $checked = array();
    foreach ($selected_nodes as $selected_node) {
        $node = array();
        foreach ($inputs as $key => $input) {
            $key = ($key == 'table') ? 'uuid' : $key;
            if (isset($input[$selected_node])) {
                $node[$key] = $input[$selected_node];
            }
        }
        $checked[] = $node;
    }

    $result = array();
    foreach ($checked as $node) {
        $node_data = drupal_json_decode($node['node_info']);
        $result[] = entity_create('node', $node_data);
    }

    $result = _api_migration_save_nodes($result);

    return $result;
}

/**
 * @param array $nodes
 * @return array
 */
function _api_migration_save_nodes($nodes)
{
    $count = array(
        'inserted' => 0,
        'updated' => 0,
    );

    foreach ($nodes as $node) {
        $node->uid = _api_migration_get_current_user($node->name);

        if (property_exists($node, 'import_files')) {
            foreach ($node->import_files as $key => $file_data) {
                if (!empty($file_data)) {
                    $save_result = _api_migration_save_file($file_data);
                    $node->$key = array(LANGUAGE_NONE => array('0' => (array)$save_result));
                }
            }
        }

        if ($node->is_new){
            $result = _api_migration_insert_node($node);

            if ($result) {
                $count['inserted']++;
            }

        } elseif ($node->node_status == 'modified') {
            $result = _api_migration_update_node($node);

            if ($result) {
                $count['updated']++;
            }
        }
    }

    return $count;
}

/**
 * @param string $name
 * @return null
 */
function _api_migration_get_current_user($name = '')
{
    $user = db_select('users', 'u')
        ->fields('u', array('uid'))
        ->condition('u.name', $name)
        ->execute()->fetchAssoc();

    return ($user) ? $user['uid'] : null;
}

/**
 * @param object $node
 * @return bool
 * @throws Exception
 */
function _api_migration_insert_node($node)
{
    $node->nid = null;
    $node->vid = null;
    
    node_save($node);

    return ($node->nid) ? true : false;
}

/**
 * @param object $node
 * @return bool
 * @throws Exception
 */
function _api_migration_update_node($node)
{
    $nid = _api_migration_get_nid($node);

    if ($nid) {
        $node->nid = $nid;

        $node->revision = TRUE;
        node_save($node);

        return true;
    } else {
        return false;
    }
}

/**
 * @param object $node
 * @return null
 */
function _api_migration_get_nid($node)
{
    $nid =  db_select('node', 'n')
        ->fields('n', array('nid'))
        ->condition('n.uuid', $node->uuid)
        ->execute()->fetchAssoc();

    return ($nid) ? $nid['nid'] : null;
}

function _api_migration_save_file($file_data)
{
    $image = file_get_contents($file_data['url']);
    $file = file_save_data($image, $file_data['scheme'] . '://' . $file_data['filename'], FILE_EXISTS_RENAME);

    $update = _api_migration_update_uuid_file($file->uuid, $file_data['uuid']);

    $result = $file;

    return $result;
}


function _api_migration_update_uuid_file($old_uuid, $new_uuid)
{
    $num_updated = db_update('file_managed') // Table name no longer needs {}
    ->fields(array(
        'uuid' => $new_uuid,
    ))
        ->condition('uuid', $old_uuid)
        ->execute();

    return $num_updated;
}

function api_migration_save_panels()
{
    $selected_panels = (isset($_POST['panels'])) ? $_POST['panels'] : array();

    foreach ($selected_panels as $selected_panel){
        $panel = $_POST['panel_info'][$selected_panel];
        $panel = json_decode($panel, true);

        if ($panel['compare_status'] === 'new') {
            _api_migration_insert_panel($panel);
        } elseif($panel['compare_status'] == 'modified') {
            _api_migration_update_panel($panel);
        }
    }

    _api_migration_info_message('Done', 'status','admin/config/content/api_migration');
}

function _api_migration_insert_panel($panel)
{
    $panels_pane = $panel['panels_pane'];
    $page_manager_pages = $panel['page_manager_pages'];
    $display = array_values($panel['display'])[0];

    unset($display['did']);
    unset($panel['compare_status']);
    unset($panel['panels_pane']);
    unset($panel['page_manager_pages']);
    unset($panel['display']);
    unset($panel['did']);

    $display_did = _api_migration_db_insert('panels_display', $display);

    $panel_conf = unserialize($panel['conf']);
    $panel_conf['did'] = $display_did;
    $panel['conf'] = serialize($panel_conf);

    $did = _api_migration_db_insert('page_manager_handlers', $panel);
    $inserted = _api_migration_select_list('page_manager_handlers', 'name', array(), array(
        array('field' => 'did', 'value' => $did)
    ));

    foreach ($inserted as $item) {
        $conf = $item->conf;
        $conf = unserialize($conf);
        $conf['did'] = $did;
        $conf = serialize($conf);
        $item->conf = $conf;

        $item_array = json_decode(json_encode($item), true);
        _api_migration_db_update('page_manager_handlers', $item_array, array(
            array('field' => 'did', 'value' => $did)));
    }

    $panel_node = array(
        'did' => $did,
        'pipeline' => 'standard',
    );

    _api_migration_db_insert('panels_node', $panel_node);

    foreach ($panels_pane as $pane)
    {
        unset($pane['pid']);

        $pane['did'] = $did;
        if ($pane['type'] !== 'node') {
            _api_migration_save_panel_pane($pane);
        } else {
            $node_info = $pane['configuration'];
            $node_info = unserialize($node_info);

            $nodes = _api_migration_select_list('node', 'nid', array(), array(
                array('field' => 'uuid', 'value' => $node_info['uuid'])
            ));

            foreach ($nodes as $key => $node) {
                $node_info['nid'] = $key;
            }

            $node_info = serialize($node_info);
            $pane['configuration'] = $node_info;

            _api_migration_save_panel_pane($pane);
        }
    }

    foreach ($page_manager_pages as $page_manager_page) {
        unset($page_manager_page['pid']);
        _api_migration_db_insert('page_manager_pages', $page_manager_page);
    }
}

function _api_migration_save_panel_pane($pane)
{
    $exist_pane = _api_migration_select_list('panels_pane', 'uuid', array(), array(
    array('field' => 'uuid', 'value' => $pane['uuid'])
));

    if (!empty($exist_pane)) {
        _api_migration_db_update('panels_pane', $pane, array(
            array('field' => 'uuid', 'value' => $pane['uuid'])
        ));
    } else {
        _api_migration_db_insert('panels_pane', $pane);
    }
}

/**
 * @param array $panel
 */
function _api_migration_update_panel($panel)
{
    $site_panels = _api_migration_panels_list(1);
    $current_panel = $site_panels['page_manager_hundlers'][$panel['name']];
    $panels_pane = $panel['panels_pane'];
    $page_manager_pages = $panel['page_manager_pages'];
    $display = array_values($panel['display'])[0];
    $did = $current_panel->did;
    $current_display = array_values($current_panel->display)[0];
    $display_did = $current_display->did;

    unset($display['did']);
    unset($panel['compare_status']);
    unset($panel['panels_pane']);
    unset($panel['page_manager_pages']);
    unset($panel['display']);
    unset($panel['did']);

    _api_migration_db_update('panels_display', $display, array(
        array('field' => 'uuid', 'value' => $display['uuid'])
    ));

    $panel_conf = unserialize($panel['conf']);
    $panel_conf['did'] = $display_did;
    $panel['conf'] = serialize($panel_conf);

    _api_migration_db_update('page_manager_handlers', $panel, array(
        array('field' => 'name', 'value' => $panel['name'])
    ));

   /* $panel_node = array(
        'did' => $did,
        'pipeline' => 'standard',
    );

    _api_migration_db_insert('panels_node', $panel_node);
*/
    foreach ($panels_pane as $pane)
    {
        unset($pane['pid']);

        $pane['did'] = $did;
        if ($pane['type'] !== 'node') {
            _api_migration_save_panel_pane($pane);
        } else {
            $node_info = unserialize($pane['configuration']);

            $nodes = _api_migration_select_list('node', 'nid', array(), array(
                array('field' => 'uuid', 'value' => $node_info['uuid'])
            ));

            foreach ($nodes as $key => $node) {
                $node_info['nid'] = $key;
            }

            $node_info = serialize($node_info);
            $pane['configuration'] = $node_info;

            _api_migration_save_panel_pane($pane);
        }
    }

    foreach ($page_manager_pages as $page_manager_page) {
        unset($page_manager_page['pid']);
        _api_migration_db_update('page_manager_pages', $page_manager_page, array(
            array('field' => 'name', 'value' => $page_manager_page['name'])
        ));
    }
    
    _api_migration_info_message('Done', 'status','admin/config/content/api_migration');
}