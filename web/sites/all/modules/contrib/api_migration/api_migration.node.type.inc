<?php

/**
 * @file
 * Functions for API
 * Return list of nodes by type
 *
 * example.com/api/post/post/retrieve?types[]=node_type
 *
 * For getting results in json format, use
 * example.com/api/post/post/retrieve.json?types[]=node_type
 */

/**
 * Callback function for node_retrieve
 * @param string $fn
 * @param array $types
 * @return mixed
 */
function _api_migration_node_retrieve($fn, $types)
{
    foreach ($types as $key => $type) {
        $types[$key] = htmlspecialchars($type);
    }

    return _api_migration_find_node_items($types);
}

/**
 * Get nodes by type
 * @param array $types
 * @param int $uuid
 * @return mixed
 */
function _api_migration_find_node_items($types = array(), $uuid = null)
{
    $query = db_select('node', 'n');
    $query->fields('n', array('uuid', 'nid', 'title', 'type', 'language', 'status'));

    if (is_array($types) && !empty($types)) {
        $query->condition('n.type', $types, 'IN');
    }

    if ($uuid != null) {
        if (is_array($uuid)) {
            $query->condition('n.uuid', $uuid, 'IN');
        } else {
            $query->condition('n.uuid', $uuid, '=');
        }
    }

    $items = $query->execute()->fetchAll();

    $nodes = array();

    foreach ($items as $item) {
        $node = node_load($item->nid);
        $nodes[] = $node;
    }

    return $nodes;
}

/**
 * @param integer $nid
 * @param array $types
 * @return array
 */
function _api_migration_get_files_by_node($nid, $types)
{
    $node = node_load($nid);
    $node_fields = array();
    
    foreach ($types as $type) {
        $field = field_get_items('node', $node, $type);

        if ($field !== FALSE) {
            list($scheme, $filepath) = explode(':', $field[0]['uri']);
            $node_fields[$type] = array(
                'uuid' => $field[0]['uuid'],
                'filename' => $field[0]['filename'],
                'scheme' => $scheme,
                'url' => file_create_url($field[0]['uri'])
            );
        }
    }

    return $node_fields;
}