<?php

/**
 * @file
 *
 * Get types form API
 */

/**
 * Implements of hook_services_resources().
 */
function api_migration_services_resources() {
    $api = array(
        'type' => array(
            'operations' => array(
                'index' => array(
                    'help' => 'List of content type',
                    'callback' => '_api_migration_types_list',
                    'access callback' => 'user_access',
                    'access arguments' => array('access content'),
                    'access arguments append' => FALSE,
                    'args' => array(
                        array(
                            'name' => 'fn',
                            'type' => 'string',
                            'description' => 'Function to perform',
                            'source' => array('path' => '0'),
                            'optional' => TRUE,
                            'default' => '0',
                        ),
                    ),
                ),
            ),
        ),
        'post' => array(
            'operations' => array(
                'retrieve' => array(
                    'help' => 'Retrieves posted nodes',
                    'callback' => '_api_migration_node_retrieve',
                    'access callback' => 'user_access',
                    'access arguments' => array('access content'),
                    'access arguments append' => FALSE,
                    'args' => array(
                        array(
                            'name' => 'fn',
                            'type' => 'string',
                            'description' => 'Function to perform',
                            'source' => array('path' => '0'),
                            'optional' => TRUE,
                            'default' => '0',
                        ),
                        array(
                            'name' => 'types',
                            'type' => 'array',
                            'description' => 'Types of node',
                            'source' => array('param' => 'types'),
                            'optional' => FALSE,
                            'default' => '',
                        ),
                    ),
                ),
            ),
            'relationships' => array(
                'files' => array(
                    'file' => array(
                        'type' => 'inc',
                        'module' => 'services',
                        'name' => 'resources/node_resource',
                    ),
                    'help' => t('This method returns files associated with a node.'),
                    'access callback' => '_node_resource_access',
                    'access arguments' => array('view'),
                    'access arguments append' => TRUE,
                    'callback' => '_api_migration_get_files_by_node',
                    'args' => array(
                        array(
                            'name' => 'nid',
                            'optional' => FALSE,
                            'source' => array('path' => 0),
                            'type' => 'int',
                            'description' => 'The nid of the node whose files we are getting',
                        ),
                        array(
                            'name' => 'types',
                            'type' => 'array',
                            'description' => 'Types of node',
                            'source' => array('param' => 'types'),
                            'optional' => FALSE,
                            'default' => '',
                        ),
                    ),
                ),
            ),
        ),
        'block' => array(
            'operations' => array(
                'index' => array(
                    'help' => 'List of blocks',
                    'callback' => '_api_migration_blocks_list',
                    'access callback' => 'user_access',
                    'access arguments' => array('access content'),
                    'access arguments append' => FALSE,
                    'args' => array(
                        array(
                            'name' => 'fn',
                            'type' => 'string',
                            'description' => 'Function to perform',
                            'source' => array('path' => '0'),
                            'optional' => TRUE,
                            'default' => '0',
                        ),
                    ),
                ),
            ),
        ),
        'panel' => array(
            'operations' => array(
                'index' => array(
                    'help' => 'List of panels',
                    'callback' => '_api_migration_panels_list',
                    'access callback' => 'user_access',
                    'access arguments' => array('access content'),
                    'access arguments append' => FALSE,
                    'args' => array(
                        array(
                            'name' => 'fn',
                            'type' => 'string',
                            'description' => 'Function to perform',
                            'source' => array('path' => '0'),
                            'optional' => TRUE,
                            'default' => '0',
                        ),
                    ),
                ),
            ),
        ),
    );

    return $api;
}

/**
 * Get all node types from current site
 * @return mixed
 */
function _api_migration_types_list()
{
    // Compose query
    $query = db_select('node_type', 'n')
        ->fields('n', array(
            'type',
            'name',
        ));


    $items = $query->execute()->fetchAll();

    return $items;
}

/**
 * Get all node types from remote server
 * @param array $source
 * @return mixed
 */
function _api_migration_get_types($source)
{
    $service_url = $source['url'] . '/' . $source['endpoint'] . '/' . 'type/index.json';
    
    $curl_response = _api_migration_get_content_with_curl($service_url, $source);

    return json_decode($curl_response);
}

/**
 * @return array
 */
function _api_migration_get_current_types()
{
    $current_types = db_select('node_type', 'n')
        ->fields('n', array(
            'name',
            'type'
        ))->execute()->fetchAll();

    $result = array();
    foreach ($current_types as $current_type)
    {
        $result[] = array(
            'name' => $current_type->name,
            'type' => $current_type->type
        );
    }

    return $result;
}