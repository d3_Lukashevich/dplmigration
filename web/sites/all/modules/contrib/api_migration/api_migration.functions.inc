<?php

/**
 * @param string $url
 * @param array $source
 * @return mixed
 */
function _api_migration_get_content_with_curl($url, $source)
{
    $options = array(
        'headers' => array(
            $source['apikey_identifier'] => $source['apikey'],
            $source['token_identifier'] => $source['token'],
        )
    );

    $response = drupal_http_request($url, $options);

    return $response->data;
}

/**
 * @param string $value
 * @return string
 */
function _api_migration_remove_slash($value)
{
    return trim($value, '/');
}

/**
 * @param string $message
 * @param string $status
 * @param string $url
 */
function _api_migration_info_message($message, $status, $url)
{
    drupal_set_message($message, $status);
    drupal_goto($url);
}

/**
 * Return class for modified fields
 * @param string $field
 * @param array $node_compare
 * @return string
 */
function _api_migration_get_field_class($field, $node_compare)
{
    return (($node_compare['status'] === 'new') ||
        (array_key_exists($field, $node_compare['fields']) && $node_compare['fields'][$field] === 'field-changed'))
        ? 'field-changed' : '';
}

/**
 * Compare two nodes and return list of modified fields, and status
 * @param stdClass $node
 * @param array $current_node
 * @return array
 */
function _api_migration_compare_two_nodes(&$node, $current_node)
{
    $compare_data = array();
    $node = _api_migration_object_to_array($node);
    $compare_data['status'] = 'new';
    $compare_data['fields'] = array();

    if ($current_node != FALSE && !empty($current_node) && isset($current_node[$node['uuid']])) {
        $compare_node = (array)$current_node[$node['uuid']];

        $compare_data['status'] = 'not modified';
        $node = _api_migration_unset_excess_node_fields($node);
        $compare_node = _api_migration_unset_excess_node_fields($compare_node);

        foreach ($compare_node as $key => $value) {
            if (array_key_exists($key, $node) && $compare_node[$key] != $node[$key]) {
                $compare_data['status'] = 'modified';
                $compare_data['fields'][$key] = 'field-changed';
            } else {
                if (array_key_exists($key, $node) && is_array($node[$key])) {
                    unset($node[$key]);
                    
                    if (isset($node['import_files'][$key])) {
                        unset($node['import_files'][$key]);
                    }
                }
            }
        }
    }

    $node = (object)$node;

    return $compare_data;
}

/**
 * @param integer $status_code
 * @return string
 */
function _api_migration_get_node_status($status_code)
{
    return ($status_code == 1) ? "published" : "not published";
}

/**
 * @param array $current_nodes
 * @return array
 */
function _api_migration_nodes_convert_to_array($current_nodes)
{
    $assoc_data = array();

    foreach ($current_nodes as $node) {
        $assoc_data[$node->uuid] = $node;
    }

    return $assoc_data;
}

/**
 * @param array $node
 * @return mixed
 */
function _api_migration_unset_excess_node_fields($node)
{
    $excess_fields = array(
        'nid', 'vid', 'vuuid', 'created', 'changed', 'revision_timestamp', 'last_comment_timestamp', 'comment'
    );
    
    $files_fields = _api_migration_get_field_with_files();

    foreach ($node as $key => $field) {
        if (is_array($node[$key]) && in_array($key, $files_fields)) {
            _api_migration_unset_file_fields($node[$key]);
        } else {
            if (in_array($key, $excess_fields)) {
                unset($node[$key]);
            }
        }
    }

    return $node;
}

/**
 * @param object $obj
 * @return array
 */
function _api_migration_object_to_array($obj) {
    if (is_object($obj)) {
        $obj = (array) $obj;
    }

    if (is_array($obj)) {
        $new = array();

        foreach ($obj as $key => $val) {
            $new[$key] = _api_migration_object_to_array($val);
        }
    } else {
        $new = $obj;
    }

    return $new;
}

function _api_migration_get_field_with_files()
{
    $files = field_read_fields(array('type' => 'file'));
    $images = field_read_fields(array('type' => 'image'));

    $result = array_merge_recursive($files, $images);

    return array_keys($result);
}

function _api_migration_unset_file_fields(&$field)
{
    $accepted_fields = array('filename');

    if ($field) {
        foreach ($field['und'][0] as $key => $field_info) {
            if (!in_array($key, $accepted_fields)) {
                unset($field['und'][0][$key]);
            }
        }
    }
}

/**
 * @param array $intersect_types
 * @return array
 */
function _api_migration_types_to_assoc_array($intersect_types)
{
    $types_data = array();

    if (!empty($intersect_types)) {
        foreach ($intersect_types as $type) {
            $types_data[$type['type']] = $type['name'];
        }
    }

    return $types_data;
}



function _api_migration_db_insert($table_name, array $values)
{
    $id = db_insert($table_name) // Table name no longer needs {}
    ->fields($values)->execute();

    return $id;
}

function _api_migration_db_update($table_name, array $values, $conditions = array())
{
    $query = db_update($table_name)->fields($values);

    foreach ($conditions as $condition) {
        $query->condition($condition['field'], $condition['value']);
    }

    return $query->execute();
}