<?php

function _api_migration_blocks_list($fn)
{
    //Table block
    $query = db_select('block', 'b');
    $result = $query
        ->fields('b')
        ->orderBy('b.region')
        ->orderBy('b.weight')
        ->orderBy('b.module')
        ->addTag('block_load')
        ->addTag('translatable')
        ->execute();

    $block_info = $result->fetchAllAssoc('bid');

    foreach ($block_info as $key => $item) {
        if (is_numeric($item->delta)) {
            unset($block_info[$key]);
        }
    }

   //Table block_custom
    $query = db_select('block_custom', 'c');
    $result = $query
        ->fields('c')
        ->execute();

    $block_custom_info = $result->fetchAllAssoc('bid');

    foreach ($block_custom_info as $key => $block) {
        $query = db_select('block', 'b');
        $result = $query
            ->where('b.delta=' . $block->bid)
            ->fields('b')
            ->execute();

        $blocks = $result->fetchAllAssoc('bid');

        $block_custom_info[$key]->blocks = $blocks;
    }

    $result = array();
    $result['block'] = $block_info;
    $result['block_custom'] = $block_custom_info;


    return $result;
}

/**
 * @param array $source
 * @return mixed
 */
function _api_migration_get_blocks($source)
{
    $service_url = $source['url'] . '/' . $source['endpoint'] . '/' . 'block/index.json';

    $curl_response = _api_migration_get_content_with_curl($service_url, $source);

    return drupal_json_decode($curl_response);
}

function api_migration_save_blocks()
{
    //var_dump($_POST);die;

    $selected_blocks = api_migration_get_saved_blocks($_POST);

    if (isset($selected_blocks['block'])) {
        _api_migration_save_blocks($selected_blocks['block'], 'block');
    }
    if (isset($selected_blocks['block_custom'])) {
        _api_migration_save_blocks($selected_blocks['block_custom'], 'block_custom');
    }


    drupal_goto('/admin/structure/block');


   /* if (!isset($_POST['table'])) {
        _api_migration_info_message('Nothing to save', 'error', 'admin/config/content/api_migration');
    }

    $count = _api_migration_get_saved_nodes($_POST);
    $message = sprintf('Import successful %s inserted %s updated', $count['inserted'], $count['updated']);
    _api_migration_info_message($message, 'status', 'admin/content');*/
}

/**
 * @param array $import_block
 * @param array $site_blocks
 * @return array
 */
function _api_migration_compare_two_blocks($import_block, $site_blocks)
{
    $compare_data = array();
    $compare_data['status'] = 'new';
    $compare_data['fields'] = array();

    if (!empty($site_blocks) && isset($site_blocks[$import_block['module'] . '_' . $import_block['delta'] . '_' . $import_block['theme']])) {
        $site_block = $site_blocks[$import_block['module'] . '_' . $import_block['delta'] . '_' . $import_block['theme']];

        $compare_data['status'] = 'not modified';

        foreach ( $site_block as $key => $item) {
            if ($import_block[$key] !== $site_block[$key]) {
                $compare_data['status'] = 'modified';
                $compare_data['fields'][$key] = 'field-changed';
            }
        }
    }

    return $compare_data;
}

/**
 * @param array $import_block
 * @param array $site_blocks
 * @return array
 */
function _api_migration_compare_two_custom_blocks($import_block, $site_blocks)
{
    $compare_data = array();
    $compare_data['status'] = 'new';
    $compare_data['fields'] = array();

    if (!empty($site_blocks) && isset($site_blocks[$import_block['info']])) {
        $site_block = $site_blocks[$import_block['info']];

        $compare_data['status'] = 'not modified';

        foreach ( $site_block as $key => $item) {
            if ($key === 'blocks') {
                if (!api_migration_compare_blocks_fields($import_block[$key], $site_block[$key])) {
                    $compare_data['status'] = 'modified';
                    $compare_data['fields'][$key] = 'field-changed';
                }
            } elseif ($import_block[$key] !== $site_block[$key] && $key !== 'bid') {
                $compare_data['status'] = 'modified';
                $compare_data['fields'][$key] = 'field-changed';
            }
        }
    }

    return $compare_data;
}

/**
 * @return array
 */
function _api_migration_get_site_blocks()
{
    $query = db_select('block', 'b');

    $result = $query
        ->fields('b')
        ->orderBy('b.region')
        ->orderBy('b.weight')
        ->orderBy('b.module')
        ->addTag('block_load')
        ->addTag('translatable')
        ->execute();

    $block_info = $result->fetchAllAssoc('bid');

    $block_data = array();

    if ($block_info) {
        foreach ($block_info as $key => $item) {
            $block_data[$item->module . '_' . $item->delta . '_' . $item->theme] = (array)$item;
        }
    }

    return $block_data;
}

function _api_migration_get_site_custom_blocks()
{
    $query = db_select('block_custom', 'c');

    $result = $query
        ->fields('c')
        ->execute();

    $block_custom_info = $result->fetchAllAssoc('bid');

    $block_data = array();

    if ($block_custom_info) {
        foreach ($block_custom_info as $key => $item) {
            $query = db_select('block', 'b');
            $result = $query
                ->where('b.delta=' . $item->bid)
                ->fields('b')
                ->execute();

            $blocks = $result->fetchAllAssoc('bid');

            foreach ($blocks as $bkey => $bitem) {
                $blocks[$bkey] = (array)$bitem;
            }

            $item->blocks = $blocks;

            $block_data[$item->info] = (array)$item;
        }
    }

    return $block_data;
}

function api_migration_get_saved_blocks($blocks = array())
{
    //block
    $select_blocks = (isset($blocks['blocks'])) ? $blocks['blocks'] : array();
    //custom block
    $select_custom_blocks = (isset($blocks['custom_blocks'])) ? $blocks['custom_blocks'] : array();

    $result = array();
    foreach ($select_blocks as $select_block) {
        $result['block'][] = $blocks['block_info'][$select_block];
    }

    foreach ($select_custom_blocks as $select_custom_block) {
        $result['block_custom'][] = $blocks['custom_block_info'][$select_custom_block];
    }

    return $result;
}

function _api_migration_save_blocks($blocks, $table_name)
{
    foreach ($blocks as $block) {
        $block = drupal_json_decode($block);
        unset($block['bid']);

        if ($block['compare_status'] == 'modified') {
            unset($block['compare_status']);
            _api_migration_update_block($block, $table_name);
        } elseif ($block['compare_status'] == 'new') {
            unset($block['compare_status']);
            _api_migration_insert_block($block, $table_name);
        }
    }

}

function _api_migration_insert_block($block, $table_name)
{
    $bid = null;
    if ($table_name == 'block_custom') {
        $blk = null;
        if (isset($block['blocks'])) {
            $blks = $block['blocks'];

            unset($block['blocks']);
            $bid = db_insert($table_name)->fields($block)->execute();

            foreach ($blks as $blk) {
                unset($blk['bid']);
                $blk['delta'] = $bid;

                _api_migration_insert_block($blk, 'block');
            }

        } else {
            $bid = db_insert($table_name)->fields($block)->execute();
        }
    } else {
        $bid = db_insert($table_name)->fields($block)->execute();
    }

    return ($bid) ? true : false;
}

function _api_migration_update_block($block, $table_name)
{
    if ($table_name == 'block') {
        $query = db_update($table_name)->fields($block);
        $query->condition('module', $block['module']);
        $query->condition('delta', $block['delta']);
        $query->condition('theme', $block['theme']);

        $num = $query->execute();

    } else {
        $old_custom_block = db_select($table_name, 'b')
            ->fields('b', array('bid'))
            ->condition('b.info', $block['info'])
            ->execute()->fetch();

        $blks = $block['blocks'];
        foreach ($blks as $blk) {
            $blk['delta'] = $old_custom_block->bid;
            unset($blk['bid']);
            _api_migration_update_block($blk, 'block');
        }

        unset($block['blocks']);
        unset($block['bid']);
        $query = db_update($table_name)->fields($block);
        $query->condition('info', $block['info']);

        $num = $query->execute();
    }

    
    return ($num);
}

/**
 * @param array $import_block
 * @param array $site_block
 * @return bool
 */
function api_migration_compare_blocks_fields($import_block, $site_block)
{
    $restruct_import_blocks = api_migration_restruct_blocks_for_compare($import_block);
    $restruct_site_blocks = api_migration_restruct_blocks_for_compare($site_block);
    

    return $restruct_import_blocks === $restruct_site_blocks;
}

/**
 * @param array $blocks
 * @return array
 */
function api_migration_restruct_blocks_for_compare($blocks)
{
    $restruct_blocks = array();

    foreach ($blocks as $block) {
        unset($block['bid']);
        unset($block['delta']);
        $restruct_blocks[$block['module'] . '_' .$block['theme']] = $block;
    }

    return $restruct_blocks;
}