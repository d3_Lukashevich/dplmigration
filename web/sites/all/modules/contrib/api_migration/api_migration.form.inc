<?php

/**
 * @file
 * Administration form
 */

/**
 * Implements of hook_menu
 * @return array
 */
function api_migration_menu()
{
    $items = array();
    
    $items['admin/config/content/api_migration'] = array(
        'title'            => 'API migration sources',
        'description'      => 'Configure the import resources list.',
        'page callback'    => '_api_migration_resources_list',
        'access arguments' => array('administer site configuration'),
    );

    $items['admin/config/content/api_migration/list'] = array(
        'title'  => 'API migration sources list',
        'type'   => MENU_DEFAULT_LOCAL_TASK,
        'weight' => 1,
    );
    
    $items['admin/config/content/api_migration/add'] = array(
        'title'            => 'Add migration source',
        'page callback'    => 'drupal_get_form',
        'page arguments'   => array('api_migration_form'),
        'access arguments' => array('administer site configuration'),
        'type'             => MENU_LOCAL_TASK,
        'weight'           => 2,
    );
    
   $items['admin/config/content/api_migration/%source/edit'] = array(
       'title'            => 'Edit migration source',
       'page callback'    => 'drupal_get_form',
       'page arguments'   => array('api_migration_form', 4),
       'access arguments' => array('administer site configuration'),
       'type'             => MENU_CALLBACK,
   );
    
    $items['admin/config/content/api_migration/%source/delete'] = array(
        'title'            => 'Delete source',
        'page callback'    => 'api_migration_delete',
        'page arguments'   => array(4),
        'access arguments' => array('administer site configuration'),
        'type'             => MENU_CALLBACK,
    );

    $items['admin/config/content/api_migration/%source/import'] = array(
        'title' => 'Select what migrate',
        'page callback'    => 'api_migration_select_import_structure',
        'page arguments' => array(4),
        'access arguments' => array('administer site configuration'),
        'type'             => MENU_CALLBACK,
    );

    $items['admin/config/content/api_migration/%source/import/nodes'] = array(
        'title' => 'Migration nodes',
        'page callback'    => 'drupal_get_form',
        'page arguments' => array('api_migration_import_form', 4),
        'access arguments' => array('administer site configuration'),
        'type'             => MENU_CALLBACK,
    );

    $items['admin/config/content/api_migration/%source/migrate/nodes'] = array(
        'title'            => 'Import nodes',
        'description'      => 'Show all imported nodes.',
        'page callback'    => 'drupal_get_form',
        'page arguments'   => array('api_migration_select_nodes_form', 4),
        'access arguments' => array('administer site configuration'),
        'type'             => MENU_CALLBACK,
        'access callback' => TRUE
    );

    $items['admin/config/content/api_migration/save/nodes'] = array(
        'title' => 'Save nodes',
        'page callback'    => 'api_migration_save_nodes',
        'access arguments' => array('administer site configuration'),
        'type'             => MENU_CALLBACK,
    );

    /**
     * Blocks
     */
    $items['admin/config/content/api_migration/%source/migrate/blocks'] = array(
        'title'            => 'Import blocks',
        'description'      => 'Show all imported blocks.',
        'page callback'    => 'drupal_get_form',
        'page arguments'   => array('api_migration_select_blocks_form', 4),
        'access arguments' => array('administer site configuration'),
        'type'             => MENU_CALLBACK,
        'access callback' => TRUE
    );

    $items['admin/config/content/api_migration/save/blocks'] = array(
        'title' => 'Save blocks',
        'page callback'    => 'api_migration_save_blocks',
        'access arguments' => array('administer site configuration'),
        'type'             => MENU_CALLBACK,
    );


    /**
     * Panels
     */
    $items['admin/config/content/api_migration/%source/migrate/panels'] = array(
        'title'            => 'Import panels',
        'description'      => 'Show all imported panels.',
        'page callback'    => 'drupal_get_form',
        'page arguments'   => array('api_migration_select_panels_form', 4),
        'access arguments' => array('administer site configuration'),
        'type'             => MENU_CALLBACK,
        'access callback' => TRUE
    );

    $items['admin/config/content/api_migration/save/panels'] = array(
        'title' => 'Save panels',
        'page callback'    => 'api_migration_save_panels',
        'access arguments' => array('administer site configuration'),
        'type'             => MENU_CALLBACK,
    );
    
    return $items;
}

/**
 * Callback for admin/config/content/api_migration
 * Show table of sources
 * @return string
 * @throws Exception
 */
function _api_migration_resources_list()
{
    $header = array(
        array('data' => t('URL')),
        array('data' => t('Endpoint')),
        array('data' => t('Description')),
        array('data' => t('Created data')),
        array('data' => t('Actions'))
    );

    $sources = db_select('api_migration', 'a')
        ->fields('a', array())
        ->execute()->fetchAll();

    $rows = array();

    if ($sources) {
        foreach ($sources as $source) {
            $actions = array(
                l(t('import'), 'admin/config/content/api_migration/' . $source->id . '/import'),
                l(t('edit'), 'admin/config/content/api_migration/' . $source->id . '/edit'),
                l(t('delete'), 'admin/config/content/api_migration/' . $source->id . '/delete'),
            );

            $rows [] = array(
                array('data' => $source->url),
                array('data' => $source->endpoint),
                array('data' => $source->description),
                array('data' => format_date($source->created_at, 'medium', 'd.m.Y H:i:s')),
                array('data' => implode(' | ', $actions)),
            );
        }
    }

    return theme('table', array(
        'header' => $header,
        'rows'   => $rows,
    ));
}

/**
 * Implements of hook_form
 * @param array $form
 * @param array $form_state
 * @param array $source
 * @return mixed
 */
function api_migration_form($form, &$form_state, $source = null)
{
    $form['url'] = array(
        '#title'         => t('Migration source url.'),
        '#description'   => t('Insert source url'),
        '#type'          => 'textfield',
        '#default_value' => $source ? $source['url'] : '',
        '#required'      => true,
    );

    $form['endpoint'] = array(
        '#title'         => t('Api endpoint.'),
        '#description'   => t('Insert api endpoint'),
        '#type'          => 'textfield',
        '#default_value' => $source ? $source['endpoint'] : '',
        '#required'      => true,
    );

    $form['apikey'] = array(
        '#title'         => t('Api key.'),
        '#description'   => t('Insert api key'),
        '#type'          => 'textfield',
        '#default_value' => $source ? $source['apikey'] : '',
        '#required'      => true,
    );

    $form['token'] = array(
        '#title'         => t('Api token.'),
        '#description'   => t('Insert api token'),
        '#type'          => 'textfield',
        '#default_value' => $source ? $source['token'] : '',
        '#required'      => true,
    );

    $form['apikey_identifier'] = array(
        '#title'         => t('Api key HTTP header.'),
        '#description'   => t('Insert api key identifier'),
        '#type'          => 'textfield',
        '#default_value' => $source ? $source['apikey_identifier'] : '',
        '#required'      => true,
    );

    $form['token_identifier'] = array(
        '#title'         => t('Api token HTTP header.'),
        '#description'   => t('Insert api token identifier'),
        '#type'          => 'textfield',
        '#default_value' => $source ? $source['token_identifier'] : '',
        '#required'      => true,
    );

    $form['description'] = array(
        '#title'         => t('Source description.'),
        '#description'   => t('Insert source description'),
        '#type'          => 'textarea',
        '#default_value' => $source ? $source['description'] : '',
        '#required'      => false,
    );

    $form['submit'] = array(
        '#type'  => 'submit',
        '#value' => $source ? t('Save') : t('Add'),
    );

    if ($source) {
        $form['id'] = array(
            '#type'  => 'value',
            '#value' => $source['id'],
        );
    }

    return $form;
}

/**
 * @param array $form
 * @param array $form_state
 */
function api_migration_form_validate($form, &$form_state)
{
    $url = $form_state['values']['url'];

    if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
        form_set_error('url', t('URL is invalid!'));
    }
}

/**
 * @param array $form
 * @param array $form_state
 */
function api_migration_form_submit($form, &$form_state)
{
    $message = '';

    $source = array(
        'url'         => _api_migration_remove_slash($form_state['values']['url']),
        'endpoint'    => _api_migration_remove_slash($form_state['values']['endpoint']),
        'description' => $form_state['values']['description'],
        'apikey' => $form_state['values']['apikey'],
        'token' => $form_state['values']['token'],
        'apikey_identifier' => $form_state['values']['apikey_identifier'],
        'token_identifier' => $form_state['values']['token_identifier'],
        'created_at'  => time()
    );

    // save edit data
    if (isset($form_state['values']['id'])) {
        $source['id'] = $form_state['values']['id'];
        drupal_write_record('api_migration', $source, 'id');
        $message = t('Migration source saved!');
    } // add new data
    else {
        drupal_write_record('api_migration', $source);
        $message = t('Migration source added!');
    }

    _api_migration_info_message($message, 'status','admin/config/content/api_migration');
}

/**
 * Get api_migration entity
 * @param integer $id
 * @return mixed
 */
function source_load($id)
{
    $query = db_select('api_migration', 'a')
        ->fields('a', array())
        ->condition('id', $id);

    return $query->execute()->fetchAssoc();
}

/**
 * Implements of hook_delete
 * @param array $source
 */
function api_migration_delete($source)
{
    $message = '';

    $source_deleted = db_delete('api_migration')
        ->condition('id', $source['id'])
        ->execute();

    if ($source_deleted) {
        $message = t('Migration source deleted!');
    } else {
        $message = t('Migration source not delete!');
    }

    _api_migration_info_message($message, 'status','admin/config/content/api_migration');
}

/**
 * @param array $form
 * @param array $form_state
 * @param array $source
 * @return mixed
 */
function api_migration_import_form($form, &$form_state, $source)
{
    $types = _api_migration_get_types($source);

    if (!is_object($types[0])) {
        _api_migration_info_message($types[0], 'error', 'admin/config/content/api_migration');
    }

    $types_data = array();
    foreach($types as $type)
    {
        $types_data[] = array(
            'name' => $type->name,
            'type' => $type->type
        );
    }

    $current_types = _api_migration_get_current_types();
    $current_types = _api_migration_types_to_assoc_array($current_types);
    $types_data = _api_migration_types_to_assoc_array($types_data);
    $intersect_types = array_intersect($current_types, $types_data);
    
    $form['types'] = array(
        '#title' => t('Content types.'),
        '#type' => 'checkboxes',
        '#options' => $intersect_types,
    );

    $form['submit'] = array(
        '#type'  => 'submit',
        '#value' => t('Next'),
    );

    $form['#action'] = url('admin/config/content/api_migration/'. $source['id'] .'/migrate/nodes');
    $form['#method'] = 'post';
    
    return $form;
}

/**
 * Implements of hook_form
 * @param array $form
 * @param array $form_state
 * @param array $source
 * @return mixed
 */
function api_migration_select_nodes_form($form, &$form_state, $source)
{
    if (!isset($_POST['types'])) {
        _api_migration_info_message('Node types not found', 'error', 'admin/config/content/api_migration');
    }
    $nodes = _api_migration_get_node_by_type($source, $_POST['types']);

    if (!is_object($nodes[0])) {
        _api_migration_info_message($nodes[0], 'error', 'admin/config/content/api_migration');
    }

    $header = array
    (
        'uuid' => t('UUID'),
        'title' => t('Title'),
        'type' => t('Type'),
        'language' => t('Language'),
        'name' => t('Name'),
        'status' => t('Status'),
        'node_status' => t('Node status'),
    );

    $options = array();
    $uuid_data = array();

    foreach ($nodes as $node) {
        $uuid_data[] = $node->uuid; // get array of all uuid for query
    }

    $form['table'] = array(
        '#type' => 'tableselect',
        '#header' => $header,
        '#empty' => t('No nodes found'),
        '#multiple' => TRUE,
    );

    $current_nodes = _api_migration_find_node_items(array(), $uuid_data);
    $current_nodes = _api_migration_nodes_convert_to_array($current_nodes);
    
    foreach ($nodes as $node) {
        $node_compare = _api_migration_compare_two_nodes($node, $current_nodes);
        $node->node_status = $node_compare['status'];

        if ($node->node_status == 'new') {
            $node->is_new = true;
        }else {
            $node->is_new = false;
        }

        $options[$node->uuid] =
            array(
            'uuid' => array(
                'data' => array(
                    '#prefix' => $node->uuid,
                ),
                'class' => _api_migration_get_field_class('uuid', $node_compare)
            ),
            'title' => array(
                'data' => array(
                    '#prefix' => $node->title,
                ),
                'class' => _api_migration_get_field_class('title', $node_compare)
            ),
            'type' => array(
                'data' => array(
                    '#prefix' => $node->type,
                ),
                'class' => _api_migration_get_field_class('type', $node_compare)
            ),
            'language' => array(
                'data' => array(
                    '#prefix' => $node->language,
                ),
                'class' => _api_migration_get_field_class('language', $node_compare)
            ),
            'name' => array(
                'data' => array(
                    '#prefix' => $node->name,
                ),
                'class' => _api_migration_get_field_class('name', $node_compare)
            ),
            'status' => array(
                'data' => array(
                    '#prefix' => _api_migration_get_node_status($node->status),
                ),
                'class' => _api_migration_get_field_class('status', $node_compare)
            ),
            'node_status' => array(
                'data' => array(
                    '#prefix' => $node_compare['status'],
                    '#name' => 'node_info[' . htmlspecialchars($node->uuid) . ']',
                    '#type' => 'hidden',
                    '#value' => drupal_json_encode($node),
                    '#attributes' => array('readonly' => 'readonly'),
                ),
                'class' => _api_migration_get_field_class('node_status', $node_compare)
            ),
        );

        if ($node->node_status === 'not modified') {
            $form['table'][$node->uuid] = array(
                '#disabled' => TRUE
            );
        }
    }

    $form['table']['#options'] = $options;

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Submit'),
    );

    $form['#action'] = url('admin/config/content/api_migration/save/nodes');
    $form['#method'] = 'post';
    
    return $form;
}

/**
 * @param array $source
 * @param array $types
 * @return mixed
 */
function _api_migration_get_node_by_type($source, $types)
{
    $url = _api_migration_get_node_api_url($source, $types);

    $json = _api_migration_get_content_with_curl($url, $source);

    $objects = json_decode($json);

    foreach($objects as $key_object => $object) {
        $file_data = _api_migration_get_files_by_api($object, $source);
        if (!empty($file_data)) {
            $object->import_files = $file_data;
        }

        $objects[$key_object] = $object;
    }
    
    return $objects;
}

/**
 * @param array $source
 * @param array $types
 * @return string
 */
function _api_migration_get_node_api_url($source, $types)
{
    $types_data = array();
    $url = $source['url'] . '/' . $source['endpoint'] . '/post/retrieve?';

    foreach ($types as $key => $type) {
        $types_data['types'][] = $type;
    }

    $parameters = drupal_http_build_query($types_data);

    return $url . $parameters;
}

function _api_migration_get_files_api_url($node, $source)
{
    $file_fields = array();

    $url = $source['url'] . '/' . $source['endpoint'] . '/post/'.$node->nid.'/files.json?';

    $fields = _api_migration_get_field_with_files();

    foreach($fields as $field) {
        $file_fields['types'][] = $field;
    }

    $parameters = drupal_http_build_query($file_fields);

    return $url . $parameters;
}

function _api_migration_get_files_by_api($node, $source)
{
    $url = _api_migration_get_files_api_url($node, $source);

    $json = _api_migration_get_content_with_curl($url, $source);

    $objects = json_decode($json);

    return $objects;
}

function api_migration_select_import_structure($source)
{
    $html = l(t('Import nodes'), 'admin/config/content/api_migration/' . $source['id'] . '/import/nodes') . '<br>';
    $html .= l(t('Import blocks'), 'admin/config/content/api_migration/' . $source['id'] . '/migrate/blocks') . '<br>';
    $html .= l(t('Import panels'), 'admin/config/content/api_migration/' . $source['id'] . '/migrate/panels');
    
    return $html;
}

function api_migration_select_blocks_form($form, &$form_state, $source)
{
    $blocks = _api_migration_get_blocks($source);

    $blocks_header = array
    (
        'module' => t('Module'),
        'delta' => t('Delta'),
        'theme' => t('Theme'),
        'region' => t('Region'),
        'title' => t('Title'),
        'status' => t('Status'),
        'compare_status' => t('Compare status'),
    );

    $custom_blocks_header = array
    (
        'info' => t('Info'),
        'body' => t('Body'),
        'format' => t('Format'),
        'compare_status' => t('Compare status'),
    );

    $block_options = array();
    $custom_block_options = array();

    $form['blocks'] = array(
        '#type' => 'tableselect',
        '#header' => $blocks_header,
        '#empty' => t('No blocks found'),
        '#multiple' => TRUE,
    );

    $form['custom_blocks'] = array(
        '#type' => 'tableselect',
        '#header' => $custom_blocks_header,
        '#empty' => t('No custom blocks found'),
        '#multiple' => TRUE,
    );
    
    $site_blocks = _api_migration_get_site_blocks();
    $custom_site_blocks = _api_migration_get_site_custom_blocks();

    foreach ($blocks['block'] as $block) {
        
        $compare_status = _api_migration_compare_two_blocks($block, $site_blocks);

        $block['compare_status'] = $compare_status['status'];

        $block_options[$block['bid']] =
            array(
                'module' => array(
                    'data' => array(
                        '#prefix' => $block['module'],
                    ),
                    'class' => _api_migration_get_field_class('module', $compare_status)
                ),
                'delta' => array(
                    'data' => array(
                        '#prefix' => $block['delta'],
                    ),
                    'class' => _api_migration_get_field_class('delta', $compare_status)
                ),
                'theme' => array(
                    'data' => array(
                        '#prefix' => $block['theme'],
                    ),
                    'class' => _api_migration_get_field_class('theme', $compare_status)
                ),
                'region' => array(
                    'data' => array(
                        '#prefix' => $block['region'],
                    ),
                    'class' => _api_migration_get_field_class('region', $compare_status)
                ),
                'title' => array(
                    'data' => array(
                        '#prefix' => $block['title'],
                    ),
                    'class' => _api_migration_get_field_class('title', $compare_status)
                ),
                'status' => array(
                    'data' => array(
                        '#prefix' => _api_migration_get_node_status($block['status']),
                    ),
                    'class' => _api_migration_get_field_class('status', $compare_status)
                ),
                'compare_status' => array(
                    'data' => array(
                        '#prefix' => $compare_status['status'],
                        '#name' => 'block_info[' . htmlspecialchars($block['bid']) . ']',
                        '#type' => 'hidden',
                        '#value' => drupal_json_encode($block),
                        '#attributes' => array('readonly' => 'readonly'),
                    ),
                    'class' => _api_migration_get_field_class('compare_status', $compare_status)
                ),
            );
    }

    foreach ($blocks['block_custom'] as $block) {

        $compare_status = _api_migration_compare_two_custom_blocks($block, $custom_site_blocks);
        $block['compare_status'] = $compare_status['status'];

        $custom_block_options[$block['bid']] =
            array(
                'info' => array(
                    'data' => array(
                        '#prefix' => $block['info'],
                    ),
                    'class' => _api_migration_get_field_class('info', $compare_status)
                ),
                'body' => array(
                    'data' => array(
                        '#prefix' => htmlspecialchars($block['body']),
                    ),
                    'class' => _api_migration_get_field_class('body', $compare_status)
                ),
                'format' => array(
                    'data' => array(
                        '#prefix' => $block['format'],
                    ),
                    'class' => _api_migration_get_field_class('format', $compare_status)
                ),
                'compare_status' => array(
                    'data' => array(
                        '#prefix' => $compare_status['status'],
                        '#name' => 'custom_block_info[' . htmlspecialchars($block['bid']) . ']',
                        '#type' => 'hidden',
                        '#value' => drupal_json_encode($block),
                        '#attributes' => array('readonly' => 'readonly'),
                    ),
                    'class' => _api_migration_get_field_class('compare_status', $compare_status)
                ),
            );
    }

    $form['blocks']['#options'] = $block_options;
    $form['custom_blocks']['#options'] = $custom_block_options;

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Submit'),
    );

    $form['#action'] = url('admin/config/content/api_migration/save/blocks');
    $form['#method'] = 'post';

    return $form;
}


function api_migration_select_panels_form($form, &$form_state, $source)
{
    $panels = _api_migration_get_panels($source);

    $panels_header = array
    (
        'name' => t('Name'),
        'task' => t('Task'),
        'subtask' => t('Subtask'),
        'handler' => t('Handler'),
        'compare_status' => t('Compare_status'),
    );

    $panels_options = array();

    $form['panels'] = array(
        '#type' => 'tableselect',
        '#header' => $panels_header,
        '#empty' => t('No blocks found'),
        '#multiple' => TRUE,
    );

    $site_panels = _api_migration_object_to_array(_api_migration_panels_list(null));

    foreach ($panels['page_manager_hundlers'] as $key => $panel) {
        $compare_status = _api_migration_compare_two_panels($panel, $site_panels['page_manager_hundlers']);

        $panel['compare_status'] = $compare_status['status'];

        $panels_options[$panel['did']] =
            array(
                'name' => array(
                    'data' => array(
                        '#prefix' => $panel['name'],
                    ),
                    'class' => _api_migration_get_field_class('name', $compare_status)
                ),
                'task' => array(
                    'data' => array(
                        '#prefix' => $panel['task'],
                    ),
                    'class' => _api_migration_get_field_class('task', $compare_status)
                ),
                'subtask' => array(
                    'data' => array(
                        '#prefix' => $panel['subtask'],
                    ),
                    'class' => _api_migration_get_field_class('subtask', $compare_status)
                ),
                'handler' => array(
                    'data' => array(
                        '#prefix' => $panel['handler'],
                    ),
                    'class' => _api_migration_get_field_class('handler', $compare_status)
                ),
                'compare_status' => array(
                    'data' => array(
                        '#prefix' => $compare_status['status'],
                        '#name' => 'panel_info[' . htmlspecialchars($panel['did']) . ']',
                        '#type' => 'hidden',
                        '#value' => drupal_json_encode($panel),
                        '#attributes' => array('readonly' => 'readonly'),
                    ),
                    'class' => _api_migration_get_field_class('compare_status', $compare_status)
                ),
            );
    }

    $form['panels']['#options'] = $panels_options;

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Submit'),
    );

    $form['#action'] = url('admin/config/content/api_migration/save/panels');
    $form['#method'] = 'post';

    return $form;
}